# eyetracking analysis

This code can be used to export (and convert) data from a csv driving session.

`extract.py` will output all occurrences of the paricipant not paying attention to the road.
Arguments to the script are explained by calling `python3 extract.py` without arguments.

Example code for using `extract.py` (expecting the csv files to be in `data/*.csv`):

```bash
for t in 1000 2000 3000; do echo $t; python3 extract.py $t 5 data/*.csv; done > 5.csv
```

`hmi.py` will cut each 30 second interval into a given amount of slices and output for each
slide if the participant was looking at the human machine interface during the slice.

Example code for using `hmi.py` (expecting the csv files to be in `data/*.csv`):

```bash
python3 hmi.py 1000 data/*.csv
```
