from csv import DictReader, DictWriter
import sys


def hmi_hit(e):
    for k in e.keys():
        if "display" in k.lower() or "hmi" in k.lower():
            return e[k]

def find_events(r):
    i = 0
    started = -1
    events = list()
    raw_events = list(r)
    for e in raw_events:
        if e.get('Event').endswith('Abkoppeln') or e.get('Event').endswith('Ankoppeln'):
            if started != -1:
                raise Exception('error A{b,n}koppeln while not expected')
            started = i
        if e.get('Event').endswith('Abkoppeln_30s') or e.get('Event').endswith('Ankoppeln_30s'):
            if started == -1:
                raise Exception('error A{b,n}koppeln_30s while not expected')
            events.append(raw_events[started:i+1])
            started = -1
        i = i + 1
    return events

def find_hmi_hits(r, name, interval_length):
    sse = find_events(r)
    hits = list()
    number_intervals = 30000 // interval_length
    for s30 in sse:
        i = 0
        started = 0
        # the event start name
        event = s30[0].get('Event')
        start = int(s30[0].get('Recording timestamp'))
        hit = False
        hit_index_count = 0
        d = dict(filename=name, participant_name=s30[0].get('Participant name'), event_name=event, start=s30[0].get('Recording timestamp'))
        for j in range(number_intervals):
            d['hmi_hit_' + str(j)] = False
        for e in s30:
            #  if hit_index_count >= 16:
                #  print("warning more than 15 intervals", file=sys.stderr)
            current = int(s30[i].get('Recording timestamp'))
            if start == -1:
                start = current
                #d['hmi_hit_' + str(hit_index_count)] = hit
                hit = False
            elif current >= start + interval_length:
                # emit
                start = current
                hit_index_count = hit_index_count + 1
            if hmi_hit(e) == '1' and hit_index_count < number_intervals:
                d['hmi_hit_' + str(hit_index_count)] = True
            i = i + 1

        hits.append(d)
    return hits

def main():
    interval_length = int(sys.argv[1])
    number_intervals = 30000 // interval_length
    print('filename,participant_name,event_name,start', end='') #,hmi_hit_0,hmi_hit_1,hmi_hit_2,hmi_hit_3,hmi_hit_4,hmi_hit_5,hmi_hit_6,hmi_hit_7,hmi_hit_8,hmi_hit_9,hmi_hit_10,hmi_hit_11,hmi_hit_12,hmi_hit_13,hmi_hit_14')
    fieldnames=['filename', 'participant_name', 'event_name', 'start']
    for i in range(number_intervals):
        print(',hmi_hit_' + str(i), end='')
        fieldnames.append('hmi_hit_' + str(i))
    print()
    for name in sys.argv[2:]:
        with open(name, newline='', encoding='utf-16') as f:
            r = DictReader(f, delimiter='\t', quotechar='"')
            tp = find_hmi_hits(r, name, interval_length)
            w = DictWriter(sys.stdout, fieldnames=fieldnames)
            w.writerows(tp)

if __name__ == '__main__':
    if len(sys.argv) < 1:
        print("Usage %s FILES..." % (sys.argv[0],), file=sys.stderr)
    main()
