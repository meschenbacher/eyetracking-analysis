from csv import DictReader, DictWriter
import sys
from enum import Enum

class State(Enum):
    LOOKING_TRAFFIC = 2
    LOOKING_AWAY = 3

def frontscheibe_hit(e):
    for k in e.keys():
        if "frontscheibe" in k.lower():
            return e[k]

def seitenspiegel_hit(e):
    for k in e.keys():
        if "seitenspiegel" in k.lower():
            return e[k]

def find_events(r):
    i = 0
    started = -1
    events = list()
    raw_events = list(r)
    for e in raw_events:
        if e.get('Event').endswith('Abkoppeln') or e.get('Event').endswith('Ankoppeln'):
            if started != -1:
                raise Exception('error A{b,n}koppeln while not expected')
            started = i
        if e.get('Event').endswith('Abkoppeln_30s') or e.get('Event').endswith('Ankoppeln_30s'):
            if started == -1:
                raise Exception('error A{b,n}koppeln_30s while not expected')
            events.append(raw_events[started:i+1])
            started = -1
        i = i + 1
    return events

def find_adjacent_lookaways(r, name, lowpass, blink_sponge):
    sponge_counter = 0
    lookaways = list()
    sse = find_events(r)
    for s30 in sse:
        s = State.LOOKING_AWAY
        i = 0
        started = 0
        sponge_counter = 0
        # the event start name
        event = s30[0].get('Event')
        eyes_not_found_counter = 0
        for e in s30:
            assert e.get('Eye movement type') is not None
            if e.get('Eye movement type') == 'EyesNotFound':
                eyes_not_found_counter = eyes_not_found_counter + 1
                if eyes_not_found_counter >= 10:
                    started = -1
                    sponge_counter = 0
                    eyes_not_found_counter = 0
                    s = State.LOOKING_TRAFFIC
            else:

                # eyes are found

                if frontscheibe_hit(e) == '1' or seitenspiegel_hit(e) == '1':
                    # now we are looking at the traffic
                    if s == State.LOOKING_AWAY: #and sponge_counter > blink_sponge:
                        t_start = int(s30[started].get('Recording timestamp'))
                        t_stop = int(s30[max(i, i - sponge_counter)].get('Recording timestamp'))
                        t_diff = t_stop - t_start
                        if t_diff < 0:
                            raise Exception('Difference negative %d - %d = %d' % (t_stop, t_start, t_diff))
                        if t_diff >= lowpass:
                            lookaways.append(dict(filename=name,
                                participant_name=s30[started].get('Participant name'),
                                event_name=event, start=t_start, end=t_stop, diff=t_diff))
                        started = -1
                    # reset sponge
                    sponge_counter = 0
                    eyes_not_found_counter = 0
                    s = State.LOOKING_TRAFFIC
                else:
                    # now we are not looking at the traffic
                    sponge_counter = sponge_counter + 1
                    if s == State.LOOKING_TRAFFIC:
                        started = i
                    if sponge_counter >= blink_sponge:
                        s = State.LOOKING_AWAY

                # reset eyes not found counter
                eyes_not_found_counter = 0
            i = i + 1
    return lookaways

def main():
    lowpass = int(sys.argv[1])
    blink_sponge = int(sys.argv[2])
    print('filename,participant_name,event_name,start,end,diff')
    for name in sys.argv[3:]:
        with open(name, newline='', encoding='utf-16') as f:
            r = DictReader(f, delimiter='\t', quotechar='"')
            tp = find_adjacent_lookaways(r, name, lowpass, blink_sponge)
            w = DictWriter(sys.stdout, fieldnames=[
                'filename', 'participant_name', 'event_name', 'start', 'end', 'diff'])
            w.writerows(tp)

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print("Usage %s LOWPASS_MILISECONDS BLINK_SPONGE FILES..." % (sys.argv[0],), file=sys.stderr)
    main()
